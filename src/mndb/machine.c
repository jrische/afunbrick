#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "mndb/machine.h"

static size_t mask(size_t wordsize)
{
    return (size_t) (wordsize == 64 ? UINT64_MAX : ((UINT64_C(1) << wordsize) - 1));
}

void afb_mndb_machine_init(struct afb_mndb_machine* machine, struct afb_config* config)
{
    machine->memsize = config->memsize ? config->memsize : 1024;
    machine->mem = calloc(machine->memsize, sizeof(*machine->mem));
    if (!machine->mem) {
        fprintf(config->err_stream, "%s: cannot allocate memory: %s", config->bin, strerror(errno));
        exit(1);
    }
    for (size_t i = 0; i < machine->memsize; i++) {
        machine->mem = calloc(machine->memsize, sizeof(*(*machine->mem)));
        if (!machine->mem[i]) {
            fprintf(config->err_stream, "%s: cannot allocate memory: %s", config->bin, strerror(errno));
            exit(1);
        }
    }
    machine->dir = MNDB_RIGHT;
    machine->i = 0;
    machine->j = 0;
    machine->mask = mask(config->wordsize);
}

void afb_mndb_machine_free(struct afb_mndb_machine* machine)
{
    for (size_t i = 0; i < machine->memsize; i++) {
        free(machine->mem[i]);
    }
    free(machine->mem);
}

void afb_mndb_machine_print_memory(struct afb_mndb_machine* machine, struct afb_config* config)
{
    size_t ncol = machine->memsize;
    fprintf(config->err_stream, "       ");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, " %3zu ", i);
    }
    fprintf(config->err_stream, "\n");
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
    for (size_t i = 0; i < machine->memsize; i++) {
        fprintf(config->err_stream,"%5zu |", i);
        size_t* tmp = machine->mem[i];
        for (size_t j = 0; j < machine->memsize; j++) {
            if ((i == machine->i) && (j == machine->j)) {
                fprintf(config->err_stream, "[%3zu]", tmp[j]);
            } else {
                fprintf(config->err_stream, " %3zu ", tmp[j]);
            }
        }
        fprintf(config->err_stream, "|\n");
    }
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
}

static size_t* get_address(struct afb_mndb_machine* machine)
{
    return &machine->mem[machine->i][machine->j];
}

static size_t get_value(struct afb_mndb_machine* machine)
{
    return *(get_address(machine));
}

static void apply_mask(struct afb_mndb_machine* machine)
{
    if (machine->mask) {
        (*(get_address(machine))) &= machine->mask;
    }
}

static void inc_value(struct afb_mndb_machine* machine)
{
    (*(get_address(machine)))++;
    apply_mask(machine);
}

static void dec_value(struct afb_mndb_machine* machine)
{
    (*(get_address(machine)))--;
    apply_mask(machine);
}

static void print_value(struct afb_mndb_machine* machine)
{
    int c = (int) get_value(machine);
    putchar(c == 10 ? '\n' : c);
}

static void read_value(struct afb_mndb_machine* machine)
{
    int c = getchar();
    if (c >= 0) {
        *(get_address(machine)) = (size_t) c;
    }
}

static void rotate(struct afb_mndb_machine* machine)
{
    (machine->dir)++;
    machine->dir %= 4;
}

static void go_right(struct afb_mndb_machine* machine)
{
    machine->j = (machine->j + 1) % machine->memsize;
}

static void go_down(struct afb_mndb_machine* machine)
{
    machine->i = (machine->i + 1) % machine->memsize;
}

static void go_left(struct afb_mndb_machine* machine)
{
    machine->j = (machine->j - 1) % machine->memsize;
}

static void go_up(struct afb_mndb_machine* machine)
{
    machine->i = (machine->i - 1) % machine->memsize;
}

static void go(struct afb_mndb_machine* machine)
{
    switch (machine->dir) {
        case MNDB_RIGHT:    go_right(machine);    break;
        case MNDB_DOWN:     go_down(machine);     break;
        case MNDB_LEFT:     go_left(machine);     break;
        case MNDB_UP:       go_up(machine);       break;
        default:
            assert(false);
    }
}

static int exec_block(struct afb_mndb_machine* machine, struct afb_bcvec* block, struct afb_config* config)
{
    int res = 0;
    for (size_t i = 0; (i < block->size) && (res >= 0); i++) {
        res |= afb_mndb_exec(machine, afb_bcvec_getp(block, i), config);
    }
    return res;
}

int afb_mndb_exec(struct afb_mndb_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    if (config->debug) {
        fprintf(config->err_stream, "%s:\n", afb_bytecode_ins_desc(bytecode->ins));
    }
    switch (bytecode->ins) {
        case INS_PROGRAM:
            return exec_block(machine, &bytecode->block, config);
        case INS_WHILE:
            while (get_value(machine) && (res >= 0)) {
                res |= exec_block(machine, &bytecode->block, config);
            }
            break;
        case INS_LEFT:  rotate(machine);        break;
        case INS_RIGHT: go(machine);            break;
        case INS_INC:   inc_value(machine);     break;
        case INS_DEC:   dec_value(machine);     break;
        case INS_WRITE: print_value(machine);   break;
        case INS_READ:  read_value(machine);    break;
        default:
            // unsupported instruction
            res = -1;
    }
    if (config->debug) {
        afb_mndb_machine_print_memory(machine, config);
    }
    return res;
}
