#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "ook/parser.h"

#define TOKEN "Ook"
#define TOKEN_LEN 4

enum ook_ins {
    OOK_RIGHT   = '.' ^ ('?' << 2),
    OOK_LEFT    = '?' ^ ('.' << 2),
    OOK_INC     = '.' ^ ('.' << 2),
    OOK_DEC     = '!' ^ ('!' << 2),
    OOK_WRITE   = '!' ^ ('.' << 2),
    OOK_READ    = '.' ^ ('!' << 2),
    OOK_START   = '!' ^ ('?' << 2),
    OOK_END     = '?' ^ ('!' << 2)
};

static size_t nearest_smaller_multiple(size_t x, size_t factor)
{
    return factor ? (x / factor) * factor : 0;
}

int afb_ook_parse(struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    int c;
    size_t i = 0;
    enum ook_ins ins = 0;
    while ((c = fgetc(config->in_stream)) != EOF)
retry: {
        size_t token_i = i % TOKEN_LEN;
        switch (token_i) {
            case 3:
                switch (c) {
                    case '.':
                    case '?':
                    case '!':
                        ins ^= (unsigned int) (c << (i / TOKEN_LEN) * 2);
                        i++;
                        break;
                    default:
                        i = nearest_smaller_multiple(i, TOKEN_LEN);
                        goto retry;
                }
                break;
            default:
                if (c == TOKEN[token_i]) {
                    i++;
                } else if (token_i != 0) {
                    i = nearest_smaller_multiple(i, TOKEN_LEN);
                    goto retry;
                }
                break;
        }
        if (i == 8) {
            struct afb_bytecode bc;
            switch (ins) {
                case OOK_RIGHT: bc.ins = INS_RIGHT; break;
                case OOK_LEFT:  bc.ins = INS_LEFT;  break;
                case OOK_INC:   bc.ins = INS_INC;   break;
                case OOK_DEC:   bc.ins = INS_DEC;   break;
                case OOK_WRITE: bc.ins = INS_WRITE; break;
                case OOK_READ:  bc.ins = INS_READ;  break;
                case OOK_START:
                    bc.ins = INS_WHILE;
                    bc.block = afb_bcvec_create(0);
                    res |= afb_ook_parse(&bc, config);
                    break;
                case OOK_END:   goto end;
            }
            afb_bcvec_push(&bytecode->block, bc);
            i = 0;
            ins = 0;
        }
    }
end:
    afb_bcvec_shrink(&bytecode->block);
    return res;
}
