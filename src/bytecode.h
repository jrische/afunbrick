enum afb_ins {
    INS_PROGRAM = 0,
    INS_LEFT = 1,
    INS_RIGHT = 2,
    INS_INC = 3,
    INS_DEC = 4,
    INS_WRITE = 5,
    INS_READ = 6,
    INS_WHILE = 7,
    INS_UP = 8,
    INS_DOWN = 9,
    INS_EXIT = 10,
};

struct afb_bytecode {
    enum afb_ins ins;
    struct afb_bcvec block;
};

const char* afb_bytecode_ins_desc(enum afb_ins ins);

void afb_bytecode_init(struct afb_bytecode* program);

void afb_bytecode_free(struct afb_bytecode* bytecode);

void afb_bytecode_write(struct afb_bytecode* bytecode, struct afb_config* config);
