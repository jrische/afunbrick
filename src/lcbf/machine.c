#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "lcbf/machine.h"

static size_t get_term_width()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return w.ws_col;
}

static size_t mask(size_t wordsize)
{
    return (size_t) (wordsize == 64 ? UINT64_MAX : ((UINT64_C(1) << wordsize) - 1));
}

void afb_lcbf_machine_init(struct afb_lcbf_machine* machine, struct afb_config* config)
{
    machine->memsize = config->memsize ? config->memsize : 1024;
    machine->mem = calloc(machine->memsize, sizeof(*machine->mem));
    if (!machine->mem) {
        fprintf(config->err_stream, "%s: cannot allocate memory: %s", config->bin, strerror(errno));
        exit(1);
    }
    machine->i = 0;
    machine->mask = mask(config->wordsize);
}

void afb_lcbf_machine_free(struct afb_lcbf_machine* machine)
{
    free(machine->mem);
}

void afb_lcbf_machine_print_memory(struct afb_lcbf_machine* machine, struct afb_config* config)
{
    size_t ncol = (get_term_width() - 8) / 5;
    if (ncol == 0) {
        ncol = 1;
    }
    fprintf(config->err_stream, "       ");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, " %3zu ", i);
    }
    fprintf(config->err_stream, " \n");
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
    {
        size_t i = 0;
        for (; i < machine->memsize; i++) {
            if ((i % ncol) == 0) {
                if (i != 0) {
                    fprintf(config->err_stream, "|\n");
                }
                fprintf(config->err_stream,"%5zu |", i);
            }
            if (i == machine->i) {
                fprintf(config->err_stream, "[%3zu]", machine->mem[i]);
            } else {
                fprintf(config->err_stream, " %3zu ", machine->mem[i]);
            }
        }
        while ((i % ncol) != 0) {
            fprintf(config->err_stream, "     ");
            i++;
        }
    }
    fprintf(config->err_stream, "|\n      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
}

static void apply_mask(struct afb_lcbf_machine* machine)
{
    if (machine->mask) {
        machine->mem[machine->i] &= machine->mask;
    }
}

static size_t get_value(struct afb_lcbf_machine* machine)
{
    return machine->mem[machine->i];
}

static void inc_value(struct afb_lcbf_machine* machine)
{
    machine->mem[machine->i]++;
    apply_mask(machine);
}

static void dec_value(struct afb_lcbf_machine* machine)
{
    machine->mem[machine->i]--;
    apply_mask(machine);
}

static void print_value(struct afb_lcbf_machine* machine)
{
    int c = (int) get_value(machine);
    putchar(c == 10 ? '\n' : c);
}

static void read_value(struct afb_lcbf_machine* machine)
{
    int c = getchar();
    if (c >= 0) {
        machine->mem[machine->i] = (size_t) c;
    }
}

static void go_left(struct afb_lcbf_machine* machine)
{
    machine->i = (machine->i - 1) % machine->memsize;
}

static void go_right(struct afb_lcbf_machine* machine)
{
    machine->i = (machine->i + 1) % machine->memsize;
}

static int exec_block(struct afb_lcbf_machine* machine, struct afb_bcvec* block, struct afb_config* config)
{
    int res = 0;
    for (size_t i = 0; (i < block->size) && (res >= 0); i++) {
        res |= afb_lcbf_exec(machine, afb_bcvec_getp(block, i), config);
    }
    return res;
}

int afb_lcbf_exec(struct afb_lcbf_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    if (config->debug) {
        fprintf(config->err_stream, "%s:\n", afb_bytecode_ins_desc(bytecode->ins));
    }
    switch (bytecode->ins) {
        case INS_PROGRAM:
            return exec_block(machine, &bytecode->block, config);
        case INS_WHILE:
            while (get_value(machine) && (res >= 0)) {
                res |= exec_block(machine, &bytecode->block, config);
            }
            break;
        case INS_LEFT:  go_left(machine);       break;
        case INS_RIGHT: go_right(machine);      break;
        case INS_INC:   inc_value(machine);     break;
        case INS_DEC:   dec_value(machine);     break;
        case INS_WRITE: print_value(machine);   break;
        case INS_READ:  read_value(machine);    break;
        default:
            // insupported instruction
            res = -1;
    }
    if (config->debug) {
        afb_lcbf_machine_print_memory(machine, config);
    }
    return res;
}
