struct afb_lcbf_machine {
    size_t memsize;
    size_t* mem;
    size_t i;
    size_t mask;
};

void afb_lcbf_machine_init(struct afb_lcbf_machine* machine, struct afb_config* config);

int afb_lcbf_exec(struct afb_lcbf_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

void afb_lcbf_machine_print_memory(struct afb_lcbf_machine* machine, struct afb_config* config);

void afb_lcbf_machine_free(struct afb_lcbf_machine* machine);
