#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "bfp3/parser.h"

int afb_bfp3_parse(struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    for (int c = fgetc(config->in_stream);
         c != EOF;
         c = fgetc(config->in_stream))
    {
        struct afb_bytecode bc;
        switch (c) {
            case '<':   bc.ins = INS_LEFT;  break;
            case '>':   bc.ins = INS_RIGHT; break;
            case '+':   bc.ins = INS_INC;   break;
            case '-':   bc.ins = INS_DEC;   break;
            case '.':   bc.ins = INS_WRITE; break;
            case ',':   bc.ins = INS_READ;  break;
            case '^':   bc.ins = INS_UP;    break;
            case 'v':   bc.ins = INS_DOWN;  break;
            case 'x':   bc.ins = INS_EXIT;  break;
            case '[':
                bc.ins = INS_WHILE;
                bc.block = afb_bcvec_create(0);
                res |= afb_bfp3_parse(&bc, config);
                break;
            case ']':
                goto end;
            default:
                res |= 1 << 0;
                continue;
        }
        afb_bcvec_push(&bytecode->block, bc);
    }
end:
    afb_bcvec_shrink(&bytecode->block);
    return res;
}
