#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "bfp3/machine.h"

static size_t mask(size_t wordsize)
{
    return (size_t) (wordsize == 64 ? UINT64_MAX : ((UINT64_C(1) << wordsize) - 1));
}

void afb_bfp3_machine_init(struct afb_bfp3_machine* machine, struct afb_config* config)
{
    sizevec tmp = sizevec_create(config->memsize ? config->memsize : 1);
    sizevec_push(&tmp, 0);
    machine->mem = sizevecvec_create(config->memsize ? config->memsize : 1);
    sizevecvec_push(&machine->mem, tmp);
    machine->i = 0;
    machine->j = 0;
    machine->mask = mask(config->wordsize);
}

void afb_bfp3_machine_free(struct afb_bfp3_machine* machine)
{
    for (size_t i = 0; i < machine->mem.size; i++) {
        sizevec_free(sizevecvec_getp(&machine->mem, i));
    }
    sizevecvec_free(&machine->mem);
}

void afb_bfp3_machine_print_memory(struct afb_bfp3_machine* machine, struct afb_config* config)
{
    size_t ncol = sizevecvec_getp(&machine->mem, 0)->size;
    fprintf(config->err_stream, "       ");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, " %3zu ", i);
    }
    fprintf(config->err_stream, "\n");
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
    for (size_t i = 0; i < machine->mem.size; i++) {
        fprintf(config->err_stream,"%5zu |", i);
        sizevec* tmp = sizevecvec_getp(&machine->mem, i);
        for (size_t j = 0; j < tmp->size; j++) {
            if ((i == machine->i) && (j == machine->j)) {
                fprintf(config->err_stream, "[%3zu]", sizevec_getv(tmp, j));
            } else {
                fprintf(config->err_stream, " %3zu ", sizevec_getv(tmp, j));
            }
        }
        fprintf(config->err_stream, "|\n");
    }
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
}

static size_t get_value(struct afb_bfp3_machine* machine)
{
    return sizevec_getv(sizevecvec_getp(&machine->mem, machine->i), machine->j);
}

static size_t* get_address(struct afb_bfp3_machine* machine)
{
    return sizevec_getp(sizevecvec_getp(&machine->mem, machine->i), machine->j);
}

static void apply_mask(struct afb_bfp3_machine* machine)
{
    if (machine->mask) {
        (*(get_address(machine))) &= machine->mask;
    }
}

static void inc_value(struct afb_bfp3_machine* machine)
{
    (*(get_address(machine)))++;
    apply_mask(machine);
}

static void dec_value(struct afb_bfp3_machine* machine)
{
    (*(get_address(machine)))--;
    apply_mask(machine);
}

static void print_value(struct afb_bfp3_machine* machine)
{
    int c = (int) get_value(machine);
    putchar(c == 10 ? '\n' : c);
}

static void read_value(struct afb_bfp3_machine* machine)
{
    int c = getchar();
    if (c >= 0) {
        sizevec_set(sizevecvec_getp(&machine->mem, machine->i), machine->j, (size_t) c);
    }
}

static void go_right(struct afb_bfp3_machine* machine)
{
    (machine->j)++;
    if (machine->j == sizevecvec_getp(&machine->mem, machine->i)->size) {
        for (size_t i = 0; i < machine->mem.size; i++) {
            sizevec* tmp = sizevecvec_getp(&machine->mem, i);
            sizevec_push(tmp, 0);
        }
    }
}

static void go_down(struct afb_bfp3_machine* machine)
{
    (machine->i)++;
    if (machine->i == machine->mem.size) {
        size_t size = sizevecvec_getp(&machine->mem, 0)->size;
        sizevec tmp = sizevec_create(size);
        for (size_t j = 0; j < size; j++) {
            sizevec_push(&tmp, 0);
        }
        sizevecvec_push(&machine->mem, tmp);
    }
}

static void go_left(struct afb_bfp3_machine* machine)
{
    if (machine->j == 0) {
        for (size_t i = 0; i < machine->mem.size; i++) {
            sizevec_insert(sizevecvec_getp(&machine->mem, i), 0, 0);
        }
    } else {
        (machine->j)--;
    }
}

static void go_up(struct afb_bfp3_machine* machine)
{
    if (machine->i == 0) {
        size_t size = sizevecvec_getp(&machine->mem, 0)->size;
        sizevec tmp = sizevec_create(size);
        for (size_t j = 0; j < size; j++) {
            sizevec_push(&tmp, 0);
        }
        sizevecvec_insert(&machine->mem, 0, tmp);
    } else {
        (machine->i)--;
    }
}

static int exec_block(struct afb_bfp3_machine* machine, struct afb_bcvec* block, struct afb_config* config)
{
    int res = 0;
    for (size_t i = 0; (i < block->size) && (res >= 0) && (res != 1); i++) {
        res |= afb_bfp3_exec(machine, afb_bcvec_getp(block, i), config);
    }
    return res;
}

int afb_bfp3_exec(struct afb_bfp3_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    if (config->debug) {
        fprintf(config->err_stream, "%s:\n", afb_bytecode_ins_desc(bytecode->ins));
    }
    switch (bytecode->ins) {
        case INS_PROGRAM:
            return exec_block(machine, &bytecode->block, config);
        case INS_WHILE:
            while (get_value(machine) && (res >= 0) && (res != 1)) {
                res |= exec_block(machine, &bytecode->block, config);
            }
            break;
        case INS_LEFT:  go_left(machine);       break;
        case INS_RIGHT: go_right(machine);      break;
        case INS_INC:   inc_value(machine);     break;
        case INS_DEC:   dec_value(machine);     break;
        case INS_WRITE: print_value(machine);   break;
        case INS_READ:  read_value(machine);    break;
        case INS_UP:    go_up(machine);         break;
        case INS_DOWN:  go_down(machine);       break;
        case INS_EXIT:  res = 1;                break;
        default:
            // insupported instruction
            res = -1;
    }
    if (config->debug) {
        afb_bfp3_machine_print_memory(machine, config);
    }
    return res;
}
