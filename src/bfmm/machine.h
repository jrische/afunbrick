struct afb_bfmm_machine {
    uint32_t mem;
    size_t i;
};

void afb_bfmm_machine_init(struct afb_bfmm_machine* machine, struct afb_config* config);

int afb_bfmm_exec(struct afb_bfmm_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

void afb_bfmm_machine_print_memory(struct afb_bfmm_machine* machine, struct afb_config* config);

void afb_bfmm_machine_free(struct afb_bfmm_machine* machine);
