enum afb_standard {
    STD_BRAINFUCK,
    STD_OOK,
    STD_BRAINFUCKPP,
    STD_BRAINFUCKMM,
    STD_PTRBRAINFUCK,
    STD_LCBF,
    STD_MNDB,
    STD_BRAINFORK,
    STD_BFP3,
};

struct afb_config {
    enum afb_standard std;
    bool print;
    bool debug;
    bool help;
    size_t wordsize;
    size_t memsize;
    const char* bin;
    const char* in_filename;
    const char* out_filename;
    const char* err_filename;
    FILE* in_stream;
    FILE* out_stream;
    FILE* err_stream;
};

void afb_config_init(struct afb_config* config);

void afb_config_free(struct afb_config* config);

int afb_config_parse(struct afb_config* config, char** argv);

int afb_config_open_streams(struct afb_config* config);

void afb_config_usage(struct afb_config* config);
