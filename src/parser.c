#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "vectors.h"
#include "config.h"
#include "bytecode.h"
#include "bf/parser.h"
#include "bfmm/parser.h"
#include "ook/parser.h"
#include "bfp3/parser.h"

#include "parser.h"

int afb_parse(struct afb_bytecode* program, struct afb_config* config)
{
    switch (config->std) {
        case STD_BRAINFUCK:
        case STD_LCBF:
        case STD_MNDB:
            return afb_bf_parse(program, config);
        case STD_BRAINFUCKMM:
            return afb_bfmm_parse(program, config);
        case STD_OOK:
            return afb_ook_parse(program, config);
        case STD_BFP3:
            return afb_bfp3_parse(program, config);
        default:
            return 1 << 0;
    }
}
