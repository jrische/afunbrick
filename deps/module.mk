DEPS_REPOS = cvector optparse
DEPS = $(DEPS_REPOS:%=.git/modules/deps/%/HEAD)

.PRECIOUS: $(DEPS)

.git/modules/deps/%/HEAD:
	git submodule init $(@:.git/modules/%/HEAD=%)
	git submodule update $(@:.git/modules/%/HEAD=%)
