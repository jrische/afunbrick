test: test.helloworld.bf test.brainfuck8bits.bf test.cellsize.bf test.hanoi.bf test.cat.bfmm test.cat.bf test.hello.bfmm test.helloworld.ook

test.cellsize.bf: $(TARGET) test/cellsize.bf
	[ "$(shell ./$(TARGET) -w 8 test/cellsize.bf 2> /dev/null)" = "8 bit cells" ]
	[ "$(shell ./$(TARGET) -w 16 test/cellsize.bf 2> /dev/null)" = "16 bit cells" ]
	[ "$(shell ./$(TARGET) -w 32 test/cellsize.bf 2> /dev/null)" = "32 bit cells" ]

test.hanoi.bf: $(TARGET) test/hanoi.bf
	./$^
	clear

test.helloworld.bf: $(TARGET) test/helloworld.bf
	[ "$(shell ./$(TARGET) test/helloworld.bf 2> /dev/null)" = "Hello World!" ]

test.brainfuck8bits.bf: $(TARGET) test/brainfuck8bits.bf
	[ "$(shell ./$(TARGET) -w 8 test/brainfuck8bits.bf 2> /dev/null)" = "brainfuck" ]

test.cat.bf: $(TARGET) test/cat.bf
	[ "$(shell echo '気持ち悪い' | ./$(TARGET) test/cat.bf 2> /dev/null)" = "気持ち悪い" ]

test.cat.bfmm: $(TARGET) test/cat.bfmm
	[ "$(shell echo '気持ち悪い' | timeout 2s ./$(TARGET) -s bf-- test/cat.bfmm 2> /dev/null)" = "気持ち悪い" ]

test.hello.bfmm: $(TARGET) test/hello.bfmm
	[ "$(shell ./$(TARGET) -s bf-- test/hello.bfmm 2> /dev/null)" = "Hello" ]

test.helloworld.ook: $(TARGET) test/helloworld.ook
	[ "$(shell ./$(TARGET) -s ook test/helloworld.ook 2> /dev/null)" = "Hello World!" ]
